import Vue from 'vue'
import Router from 'vue-router'
import ListBill from '@/components/Bill/ListBill'
import CreateBill from '@/components/Bill/CreateBill'
import EditBill from '@/components/Bill/EditBill'
import CreateUser from '@/components/User/CreateUser'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ListBillIndex',
      component: ListBill
    },
    {
      path: '/bill',
      name: 'ListBill',
      component: ListBill
    },
    {
      path: '/bill/create',
      name: 'CreateBill',
      component: CreateBill
    },
    {
      path: '/bill/edit/:id',
      name: 'EditBill',
      component: EditBill
    },
    {
      path: '/user/create',
      name: 'CreateUser',
      component: CreateUser
    }
  ]
})
