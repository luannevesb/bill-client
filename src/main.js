// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import App from './App'
import router from './router'
import VueSidebarMenu from 'vue-sidebar-menu'
import NavBar from '@/components/NavBar/NavBar'
import VueMaterial from 'vue-material'
import VeeValidate, { Validator } from 'vee-validate';
import DatePicker from 'vue2-datepicker'
import axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2';
// eslint-disable-next-line camelcase
import pt_BR from 'vee-validate/dist/locale/pt_BR';
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674'
}

Vue.use(VueSweetalert2, options);
Vue.use(VeeValidate)
Validator.localize('pt-BR', pt_BR);
Vue.use(VueMaterial)
Vue.use(VueSidebarMenu)
Vue.config.productionTip = false

const http = axios.create({
  baseURL: 'http://localhost:3000/',
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json'
  }
})

Vue.prototype.$axios = http
Vue.component('nav-bar', NavBar);
Vue.component('date-picker', DatePicker);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
