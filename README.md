# bill-client

### Tecnologias
* Vue v2.5.2
* Vue Material v1.0.0-beta-10.2
* Vue SweetAlert2 v1.6.4 
* Vue Axios v2.1.4
* Axios v0.18.0 

### Instalação
    > git clone https://luannevesb@bitbucket.org/luannevesb/bill-client.git

    > cd bill-client

    > npm install

### Execução
    # Inicia a Aplicação
    > npm run dev
